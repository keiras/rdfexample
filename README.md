# README #
* Eclipse projekt.
* Vyžaduje připojit knihovny [Apache Jena](https://jena.apache.org/download/index.cgi).

### Jak zprovoznit projekt v Eclipse? ###

Naimportovat projekt:

* File -> Import -> Git -> Projects from Git
    * Clone URI
    * do URI vložit: https://bitbucket.org/keiras/rdfexample.git
    * vybrat master větev
    * zvolit adresář k uložení na disk
    * "Import existing Eclipse project" zaškrtnout
    * vybrat rdfDemo projekt

Založit Jena user library:

* Rozbalit [apache-jena-3.1.0.zip](https://jena.apache.org/download/index.cgi) do stálého adresáře
    * Window -> Preferences -> Jana -> Build Path -> User Libraries -> New
        * libovolné jméno, nezaškrtávat System library
    * označit nově vytvořenou knihovnu -> Add external JARs... -> označit vše v apache-jena-3.1.0\lib\
    * po přidání se objeví seznam jar souborů ve vaší knihovně
    * postupně rozkliknout jena-arq, jena-cmds, jena-core a jena-tdb a double klik na Source attachment -> External location -> External File -> vybrat příslušný apache-jena-3.1.0\lib-src\

Přiřadit knihovnu do build path projektu:

* pravý klik na projekt -> Build Path -> Add Libraries -> User Library -> zaškrtnout Jena

Projekt by teď již neměl hlásit chybu při kompilaci.